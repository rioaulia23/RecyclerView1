package id.sch.smktelkom.recyclerview1.model;

import android.graphics.drawable.Drawable;

/**
 * Created by Rio Aulia Yahya on 2/15/2018.
 */

public class Hotel {
    public String judul, deskripsi;
    public Drawable foto;

    public Hotel(String judul, String deskripsi, Drawable foto) {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.foto = foto;
    }
}
